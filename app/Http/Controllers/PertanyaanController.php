<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Console\Presets\React;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class PertanyaanController extends Controller
{
    public function index()
    {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('pertanyaan'));
    }
    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);


        $query = DB::table('pertanyaan')->insert(
            [
                'judul' => $request['judul'],
                'isi' => $request['isi'],
                'tanggal_dibuat' => Carbon::now()->format('Y-m-d H:i:s'),
                'tanggal_diperbaharui' => Carbon::now()->format('Y-m-d H:i:s'),
                'jawaban_tepat_id' => 1,
                'profile_id' => 1,
            ]
        );

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan');
    }

    public function show($pertanyaan_id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function edit($pertanyaan_id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($pertanyaan_id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'tanggal_diperbaharui' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil di-perbaharui');
    }

    public function destroy($pertanyaan_id)
    {
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil di-hapus');
    }
}
