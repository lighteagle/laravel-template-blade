@extends('layouts.master')

@section('title')
List Pertanyaan
@endsection


@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">List Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('success'))
          <div class="alert alert-success"> {{session('success')}}</div>
        @endif
        <a href="/pertanyaan/create" class="btn btn-primary mb-2"> Buat Pertanyaan Baru</a>
      <table class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 10px">#</th>
            <th>Judul</th>
            <th>Isi Pertanyaan</th>
            <th style="width: 40px">Aksi</th>
          </tr>
        </thead>
        <tbody>
            @forelse($pertanyaan as $key=> $pt)
                
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$pt->judul}}</td>
                <td>{{$pt->isi}}</td>
                <td class="d-flex">
                    <a href="/pertanyaan/{{$pt->id}}" class="btn btn-info btn-sm">show</a>
                    <a href="/pertanyaan/{{$pt->id}}/edit" class="btn btn-warning btn-sm ml-2">edit</a>
                    <form action="/pertanyaan/{{$pt->id}}" method="POST" >
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm ml-2">
                    </form>
                </td>              
            </tr>

            @empty
                <tr><td colspan="4" align="left">No Data</td></tr>
            @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
    
  </div>
@endsection
