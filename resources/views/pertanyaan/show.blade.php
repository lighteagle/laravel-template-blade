@extends('layouts.master')

@section('title')
Pertanyaan
@endsection

@section('content')
<h4>{{$pertanyaan->judul}}</h4>
<p>{{$pertanyaan->isi}}</p>
@endsection