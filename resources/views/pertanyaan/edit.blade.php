@extends('layouts.master')

@section('title')
Edit Pertanyaan
@endsection

@section('content')
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-dark">
        <div class="card-header">
            <h3 class="card-title">Edit Pertanyaan (id: #{{$pertanyaan->id}})</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $pertanyaan->judul) }}" placeholder="Masukkan Judul Pertanyaan">
                    @error('judul')
                    <p class="text text-danger"> {{ $message }}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Isi Pertanyaan</label>
                    <input type="text" class="form-control"  placeholder="Tulis Pertanyaan ..." name="isi" id="isi" value="{{ old('isi', $pertanyaan->isi) }}" ></input>
                    @error('isi')
                    <p class="text text-danger"> {{ $message }}</p>
                    @enderror
                </div>

            </div>

            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
    <!-- /.card -->







</div>
@endsection