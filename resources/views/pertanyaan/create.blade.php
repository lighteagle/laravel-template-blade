@extends('layouts.master')

@section('title')
Create Pertanyaan
@endsection

@section('content')
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-dark">
        <div class="card-header">
        <h3 class="card-title">Pertanyaan </h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Masukkan Judul Pertanyaan">
                    @error('judul')
                    <p class="text text-danger"> {{ $message }}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Isi Pertanyaan</label>
                    <input type="text" class="form-control"  placeholder="Tulis Pertanyaan ..." name="isi" id="isi" value="{{ old('isi', '') }}" ></input>
                    @error('isi')
                    <p class="text text-danger"> {{ $message }}</p>
                    @enderror
                </div>

            </div>

            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
    <!-- /.card -->







</div>
@endsection